'use strict';

// Define the `myApp` module
var app = angular.module('myApp', [
  'ngAnimate',
  'ngRoute'
]);