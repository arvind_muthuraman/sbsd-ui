'use strict';

// Define the `myApp` module
var app = angular.module('myApp')
        .config(['$routeProvider',
            function config($routeProvider) {
                $routeProvider.
                when('/aboutus', {
                    templateUrl: 'sbsd.views/aboutUs/aboutUs.html',
                    replace: true,
                    controller: 'AboutUsController'
                }).when('/home', {
                    templateUrl: 'sbsd.views/home/home.html',
                    replace: true,
                    controller: 'HomeController'
                }).when('/swamcert', {
                    templateUrl: 'sbsd.views/swamCertification/swamCertification.html',
                    replace: true,
                    controller: 'SwamcertController'
                }).when('/dbecert', {
                    templateUrl: 'sbsd.views/dbeCertification/dbeCertification.html',
                    replace: true,
                    controller: 'DbecertController'
                }).when('/SWaMSearchSub', {
                    templateUrl: 'sbsd.views/swamVendorSpeach/swamVendorSpeach.html',
                    replace: true,
                    controller: 'SWaMSearchSubController'
                }).when('/DBESearchSub', {
                    templateUrl: 'sbsd.views/dbeVendorSearch/dbeVendorSearch.html',
                    replace: true,
                    controller: 'DBESearchSubController'
                }).when('/swampurchasingandexpenditure', {
                    templateUrl: 'sbsd.views/swamreports/swamreports.html',
                    replace: true,
                    controller: 'SwampurchasingandexpenditureController'
                }).when('/bis', {
                    templateUrl: 'sbsd.views/businessInformationServices/businessInformationServices.html',
                    replace: true,
                    controller: 'BisController'
                }).when('/VSBFA', {
                    templateUrl: 'sbsd.views/smallBusinessFinance/smallBusinessFinance.html',
                    replace: true,
                    controller: 'VSBFAController'
                }).when('/programs', {
                    templateUrl: 'sbsd.views/programs/programs.html',
                    replace: true,
                    controller: 'ProgramsController'
                }).when('/forms', {
                    templateUrl: 'sbsd.views/specialRequestForm/specialRequestForm.html',
                    replace: true,
                    controller: 'FormsController'
                }).when('/bid', {
                    templateUrl: 'sbsd.views/procurementAndBusiness/procurementAndBusiness.html',
                    replace: true,
                    controller: 'BidController'
                }).when('/reports_newsletters', {
                    templateUrl: 'sbsd.views/reports/reports.html',
                    replace: true,
                    controller: 'ReportsNewslettersController'
                }).when('/faq', {
                    templateUrl: 'sbsd.views/frequentlyAskedQuestion/frequentlyAskedQuestion.html',
                    replace: true,
                    controller: 'FaqController'
                }).when('/links', {
                    templateUrl: 'sbsd.views/newsAndResourse/newsAndResourse.html',
                    replace: true,
                    controller: 'LinksController'
                }).when('/contactus', {
                    templateUrl: 'sbsd.views/eightLocationAcross/eightLocationAcross.html',
                    replace: true,
                    controller: 'ContactusController'
                }).when('/searchResults', {
                    templateUrl: 'sbsd.views/search/search.html',
                    replace: true,
                    controller: 'SearchController'
                }).otherwise('/home', {
                    redirectTo: '/home'
                });
            }
        ]);
app.controller('AboutUsController', function ($scope) {
    
});
app.controller('HomeController', function ($scope) {

});
app.controller('SwamcertController', function ($scope) {

});
app.controller('DbecertController', function ($scope) {

});
app.controller('SWaMSearchSubController', function ($scope) {

});
app.controller('DBESearchSubController', function ($scope) {

});
app.controller('SwampurchasingandexpenditureController', function ($scope) {

});
app.controller('BisController', function ($scope) {

});
app.controller('VSBFAController', function ($scope) {

});
app.controller('ProgramsController', function ($scope) {

});
app.controller('FormsController', function ($scope) {

});
app.controller('BidController', function ($scope) {

});
app.controller('ReportsNewslettersController', function ($scope) {

});
app.controller('FaqController', function ($scope) {

});
app.controller('LinksController', function ($scope) {

});
app.controller('ContactusController', function ($scope) {

});
app.controller('SearchController', function ($scope) {

});


